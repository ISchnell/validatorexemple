/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package net.tncy.isc.validator;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
/**
 *
 * @author student
 */
public class ISBNValidatorTest {
    ISBNValidator validator=new ISBNValidator();
    @Test
    public void testNullAndEmptyString()throws Exception{
        isValid("");
        isValid(null);
    }
    private void isValid(String s)
    {
        assertTrue("Expected a valid ISBN", validator.isValid(s, null));
    }
}
